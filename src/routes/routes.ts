/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Root enry level file forr bootstarting node js application
 */

import express from "express";
import AccountController from "../mapping/AccountController";
import {PostRespBusinessObjects} from "../mapping/bussObj/Response";

const router: any = express.Router();

    
router.get('/account/:id', (req:any, res: any) => {    
    res.set("Content-Type","application/json; charset=utf-8");
    if(req.query.type===undefined || (req.query.type !== 'cif' && req.query.type !== 'acc')){        
        res.status(404);
        res.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9404","Invalid Request!"));
    }else{        
        let acctController!: AccountController;
        if(req.query.type==="cif"){ // ?type=cif
            acctController = new AccountController("",req.params.id);
        }else if(req.query.type==="acc"){ // ?type=acc
            acctController = new AccountController(req.params.id);
        }else{            
            res.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8404","Account Not found"));
        }
        acctController.getAccountDetail().then((response: String)=> {
            res.send(response);
        }).catch((error:any) => {
            res.send(error);
        });
        
    }    
});

/**
 * Routes Definition for health, readiness and liveness check
 */
 // Route for liveness prone
 // The kubelet kills the container and restarts it.
router.get('/healthz',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    res.status(200);    
    res.send();
});


// Route for rediness check. 
// This route will return 200 in-case all required bootstarap is finished
// Note: We want to suspend traffic in-case there is something wrong here
router.get('/readiness',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    res.status(200);
    res.send();
});


// Protect slow starting containers with startup probes
router.get('/startup',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    res.status(200);
    res.send();
});

export default router;