import AccountController from "../mapping/AccountController";

describe('Accounts Controller Test Case', () => {
    test('Account Controller instantiated with account id', () => {
        let accountController:AccountController = new AccountController('123');        
        expect(accountController.accountID).toBe('123');
    });

    test('Simulate Account fetching 8404 Account not found', () => {
        let accountController:AccountController = new AccountController('456');
        expect(accountController.accountID).toBe('456');
        accountController.getAccountDetail().then((response:String) => {
            expect(response).toBe('Not implemented yet');
        }).catch((error:any) => {            
            expect(JSON.parse(error).metadata.status).toBe("8404");
        });
    });

    test('Simulate Account fetching 0000 By account number', () => {
        let accountController:AccountController = new AccountController('80200110203355');
        expect(accountController.accountID).toBe('80200110203355');
        accountController.getAccountDetail().then((response:String) => {            
            expect(JSON.parse(response.toString()).metadata.status).toBe("0000");
            expect(response).toBeDefined();
        }).catch((error:any) => {            
            fail(error);
        });
    });

    test('Simulate Account fetching 0000 By customer number', () => {
        let accountController:AccountController = new AccountController("","123457");
        expect(accountController.accountID).toBe('');
        expect(accountController.customerID).toBe('123457');
        accountController.getAccountDetail().then((response:String) => {            
            expect(JSON.parse(response.toString()).metadata.status).toBe("0000");
            expect(response).toBeDefined();
        }).catch((error:any) => {            
            fail(error);
        });
    });
});