import {PostRespBusinessObjects} from "../../mapping/bussObj/Response";
import AccountResponseBO,{AccountBO} from "../../mapping/bussObj/Account";

describe('Accounts Controller Test Case', () => {
    test('Instantiate AccountResponse and validate the object', () => {
        let accountResp:AccountBO = new AccountBO("1234567890987","123456789098765432123445",
                    "Enabled",new Date(),"AED","Savings","Normal","Muhammad Mushahid", new Date());        
        expect(accountResp).toBeDefined();
        expect(accountResp.id).toBe('1234567890987');
        expect(accountResp.iban).toBe('123456789098765432123445');
    });

    test('Instantiate AccountResponseBO', ()=> {
        let metadata:PostRespBusinessObjects.Metadata = new PostRespBusinessObjects.Metadata();
        metadata.status = "0000";
        metadata.description = "UT001";
        metadata.responseTime = new Date();

        let accountResp:AccountBO = new AccountBO("1234567890987","123456789098765432123445",
                    "Enabled",new Date(),"AED","Savings","Normal","Muhammad Mushahid", new Date());
        let acctResponseBO:AccountResponseBO = new AccountResponseBO(metadata,accountResp);
        expect(acctResponseBO).toBeDefined();
    });

    test('Check properties', () => {
        let metadata:PostRespBusinessObjects.Metadata = new PostRespBusinessObjects.Metadata();
        metadata.status = "0000";
        metadata.description = "UT001";
        metadata.responseTime = new Date();

        let accountResp:AccountBO = new AccountBO("1234567890987","123456789098765432123445",
                    "Enabled",new Date(),"AED","Savings","Normal","Muhammad Mushahid", new Date());
        let acctResponseBO:AccountResponseBO = new AccountResponseBO(metadata,accountResp);
        expect(JSON.parse(acctResponseBO.generateJSON().toString())).toBeDefined();
    });
});