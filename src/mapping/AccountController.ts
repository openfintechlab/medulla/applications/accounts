import accountsJSON from "./simulator/accounts.json";
import {PostRespBusinessObjects} from "./bussObj/Response";
import AccountResponseBO,{AccountBO} from "./bussObj/Account";

export default class AccountController{
    private _accountID!: string;
    private _customerID!: string;

    constructor(accountID:string, customerID?: string){
        this._accountID = accountID;
        if(customerID !== undefined){
            this._customerID = customerID;
        }        
    }

    public getAccountDetail(): Promise<String>{
        return new Promise<String>((resolve:any, reject:any) => {
            let metadata:PostRespBusinessObjects.Metadata = new PostRespBusinessObjects.Metadata();
            for(let index in accountsJSON.accounts){
                if(this._customerID !== undefined){
                    if(accountsJSON.accounts[index].cif === this._customerID){                        
                        metadata.status = "0000";
                        metadata.description = "Success!";
                        metadata.responseTime = new Date();

                        let accountResp:AccountBO = new AccountBO(accountsJSON.accounts[index].id,
                                                                    accountsJSON.accounts[index].iban,
                                                                    accountsJSON.accounts[index].status,
                                                                    new Date(accountsJSON.accounts[index]["status-updated-datetime"]),
                                                                    accountsJSON.accounts[index].currency,
                                                                    accountsJSON.accounts[index].type,
                                                                    accountsJSON.accounts[index].subtype,
                                                                    accountsJSON.accounts[index].title,
                                                                    new Date(accountsJSON.accounts[index]["opening-date"]));
                        let acctResponseBO:AccountResponseBO = new AccountResponseBO(metadata,accountResp);
                        resolve(acctResponseBO.generateJSON());
                    }
                }else{
                    // Code redundancy... can be converted to a seperate function. 
                    if(accountsJSON.accounts[index].id === this._accountID){                        
                        metadata.status = "0000";
                        metadata.description = "Success!";
                        metadata.responseTime = new Date();

                        let accountResp:AccountBO = new AccountBO(accountsJSON.accounts[index].id,
                                                                    accountsJSON.accounts[index].iban,
                                                                    accountsJSON.accounts[index].status,
                                                                    new Date(accountsJSON.accounts[index]["status-updated-datetime"]),
                                                                    accountsJSON.accounts[index].currency,
                                                                    accountsJSON.accounts[index].type,
                                                                    accountsJSON.accounts[index].subtype,
                                                                    accountsJSON.accounts[index].title,
                                                                    new Date(accountsJSON.accounts[index]["opening-date"]));
                        let acctResponseBO:AccountResponseBO = new AccountResponseBO(metadata,accountResp);
                        resolve(acctResponseBO.generateJSON());
                    }
                }                
            }
            reject(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8404","Account Not found"));            
        });        
    }    

    get accountID():string{
        return this._accountID;
    }

    get customerID():string{
        return this._customerID;
    }
}