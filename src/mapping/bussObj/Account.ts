import {PostRespBusinessObjects} from "./Response";


interface Account {
    id:                        string;
    iban:                      string;
    status:                    string;
    status_updated_datetime:   Date;
    currency:                  string;
    type:                      string;
    subtype:                   string;
    title:                     string;
    opening_date:              Date;
}

export class AccountBO implements Account{
    id: string;
    iban: string;
    status: string;
    status_updated_datetime: Date;
    currency: string;
    type: string;
    subtype: string;
    title: string;
    opening_date: Date

    constructor(id:string,iban:string,status:string,status_updated_datetime: Date,currency:string,type:string,
                subtype:string,title:string, opening_date: Date){
        this.id = id;
        this.iban = iban;
        this.status = status;
        this.status_updated_datetime = status_updated_datetime;
        this.currency = currency;
        this.type = type;
        this.subtype = subtype;
        this.title = title;
        this.opening_date = opening_date;
    }

}


export default class AccountResponseBO  {
    metadata: PostRespBusinessObjects.Metadata;
    accounts: AccountBO[];

    constructor(metadata:PostRespBusinessObjects.Metadata,...accounts: AccountBO[]){        
        this.metadata = metadata;     
        this.accounts = accounts;     
    }   

    public generateJSON():String{
        return JSON.stringify(this);
    }    

}